package auth

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/noon-go/noonhttp"
)

type (
	// AuthenticateEntity entity
	AuthenticateEntity struct {
		Client      *noonhttp.ClientEntity
		BifrostHost string
	}

	// Success AuthenticateEntity
	Success struct {
		UserID int    `json:"userId"`
		Valid  string `json:"valid"`
	}

	Error struct {
		Message    string
		StatusCode int
	}

	bifrostRequest struct {
		queryParams  map[string]string
		headerParams map[string]string
		token        string
	}
)

func (e *Error) Error() string {
	return e.Message
}

const (
	authorization = "Authorization"
	clientTime    = "x-client-time"
	deviceID      = "x-device-id"
	country       = "country"
	roleConstant  = "role"
)

const (
	internalServerError = "internalServerError"
	clientTimeRequired  = "clientTimeRequired"
	deviceIDRequired    = "deviceIdRequired"
	noPermission        = "noPermission"
	notAuthorized       = "notAuthorized"
	countryIDRequired   = "countryIdRequired"
	tokenExpired        = "tokenExpired"
	clientTimeInvalid   = "clientTimeInvalid"
)

var allowedRoles = map[string]bool{
	"student": true,
	"teacher": true,
	"generic": true,
	"admin":   true,
	"supply":  true,
}

func validateRole(roles string) bool {
	rolesList := strings.Split(roles, ".")
	for _, role := range rolesList {
		if allowedRoles[role] {
			return true
		}
	}
	return false
}

func (br *bifrostRequest) validateAndPopulate(roles string, headers http.Header) error {
	queryParams := map[string]string{}
	headerParams := map[string]string{}

	tokens := strings.Split(headers.Get(authorization), " ")
	if len(tokens) <= 1 {
		return &Error{
			StatusCode: 401,
			Message:    noPermission,
		}
	}
	br.token = tokens[1]

	isValidRole := validateRole(roles)
	if !isValidRole {
		return &Error{
			StatusCode: 401,
			Message:    noPermission,
		}
	}
	queryParams[roleConstant] = roles

	countryHeaderString := headers.Get(country)
	_, e := strconv.Atoi(countryHeaderString)
	if e != nil {
		return &Error{
			StatusCode: 401,
			Message:    countryIDRequired,
		}
	}
	queryParams[country] = countryHeaderString
	br.queryParams = queryParams

	clientTimeHeader := headers.Get(clientTime)
	if clientTimeHeader == "" {
		return &Error{
			StatusCode: 401,
			Message:    clientTimeRequired,
		}
	}
	headerParams[clientTime] = clientTimeHeader

	deviceIDHeader := headers.Get(deviceID)
	if deviceIDHeader == "" {
		return &Error{
			StatusCode: 401,
			Message:    deviceIDRequired,
		}
	}
	headerParams[deviceID] = deviceIDHeader
	br.headerParams = headerParams
	return nil
}

func checkClientErrorResponse(err *noonhttp.ClientError) error {
	switch err.StatusCode {
	case 401:
		return &Error{
			StatusCode: 401,
			Message:    noPermission,
		}
	case 403:
		return &Error{
			StatusCode: 403,
			Message:    notAuthorized,
		}
	case 418:
		return &Error{
			StatusCode: 418,
			Message:    clientTimeInvalid,
		}
	case 430:
		return &Error{
			StatusCode: 430,
			Message:    tokenExpired,
		}
	default:
		return &Error{
			StatusCode: 401,
			Message:    noPermission,
		}
	}
}

// Process authentication
func (entity *AuthenticateEntity) Process(roles string, headers http.Header) (*Success, error) {
	request := &bifrostRequest{}
	success := Success{}

	err := request.validateAndPopulate(roles, headers)
	if err != nil {
		return nil, err
	}

	endpoint := entity.BifrostHost + "/bifrost/v1/access_token/" + request.token
	response, err := entity.Client.ServeGet(endpoint, request.headerParams, request.queryParams)
	if err != nil {
		if e, ok := err.(*noonhttp.ClientError); ok {
			err = checkClientErrorResponse(e)
			return nil, err
		}
		return nil, err
	}

	parsingErr := json.Unmarshal(response, &success)
	if parsingErr != nil {
		return nil, parsingErr
	}
	return &success, nil
}

// GetHTTPStatusCode return status code to send
func (e *Error) GetHTTPStatusCode() int {
	switch e.Message {
	case internalServerError:
		return http.StatusInternalServerError
	case clientTimeRequired:
		return http.StatusBadRequest
	case deviceIDRequired:
		return http.StatusBadRequest
	case countryIDRequired:
		return http.StatusBadRequest
	case noPermission:
		return http.StatusUnauthorized
	case notAuthorized:
		return http.StatusForbidden
	case clientTimeInvalid:
		return 418
	case tokenExpired:
		return 430
	default:
		return http.StatusInternalServerError
	}
}
